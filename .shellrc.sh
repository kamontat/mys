# shellcheck disable=SC1090,SC2148

export MYS_IS_EXCEPTION=false                      # Is exception occurred
export MYS_EXCEPTIONS=()                           # EXCEPTIONS constants

export MYS_SHELL_TYPE=${MYS_SHELL_TYPE:-'SHELL'}   # SHELL || ZSH || BASH
export MYS_TYPE=${MYS_TYPE:-'FULLY'}               # FULLY || MINIMAL

export MYS_RC_NAME=${MYS_RC_NAME:-'.shellrc'}      # .shellrc || .bashrc || .zshrc
export _MYS_RC_NAME=${_MYS_RC_NAME:-'.shellrc.sh'} # alternative rc name (for testing)
export MYS_IS_PRODUCTION=false                     # is mys on production shell setting

_MYS_ROOT="${HOME%%/}/${MYS_RC_NAME}"
if [ -h "$_MYS_ROOT" ]; then
  echo "link"
  MYS_IS_PRODUCTION=true
  _MYS_ROOT="$(readlink "$_MYS_ROOT")"
else
  MYS_IS_PRODUCTION=false
  _MYS_ROOT="${PWD%%/}/${_MYS_RC_NAME}" # local path
fi
MYS_ROOT="$(dirname "$_MYS_ROOT")"

if ! test -d "$MYS_ROOT"; then
  MYS_IS_EXCEPTION=true
  MYS_EXCEPTIONS+=("$MYS_ROOT is not exist")
fi

MYS_CONST_FOLDER="${MYS_ROOT%%/}/const"
if ! test -d "$MYS_CONST_FOLDER"; then
  MYS_IS_EXCEPTION=true
  MYS_EXCEPTIONS+=("$MYS_CONST_FOLDER is not exist")
fi

MYS_DEFAULT_FOLDER="${MYS_ROOT%%/}/default"
if ! test -d "$MYS_DEFAULT_FOLDER"; then
  MYS_IS_EXCEPTION=true
  MYS_EXCEPTIONS+=("$MYS_DEFAULT_FOLDER is not exist")
fi

MYS_LIB_FOLDER="${MYS_ROOT%%/}/lib"
if ! test -d "$MYS_LIB_FOLDER"; then
  MYS_IS_EXCEPTION=true
  MYS_EXCEPTIONS+=("$MYS_LIB_FOLDER is not exist")
fi

MYS_PERSONAL_FOLDER="${MYS_ROOT%%/}/personal"
if ! test -d "$MYS_PERSONAL_FOLDER"; then
  MYS_IS_EXCEPTION=true
  MYS_EXCEPTIONS+=("$MYS_PERSONAL_FOLDER is not exist")
fi

export MYS_CONST_FOLDER
export MYS_LIB_FOLDER
export MYS_DEFAULT_FOLDER
export MYS_PERSONAL_FOLDER

source "${MYS_DEFAULT_FOLDER}/.path.sh"     # load path configuration
source "${MYS_DEFAULT_FOLDER}/.variable.sh" # load default shell variable
source "${MYS_CONST_FOLDER}/initial.sh"     # load setup configuration
source "${MYS_LIB_FOLDER}/progress.sh"      # load progressbar

__mys__pg_start

__mys__pg_update "Libraries" "Load helper method"
source "${MYS_LIB_FOLDER}/helper.sh" || __mys__pg_fail "Loading helper file"
source "${MYS_LIB_FOLDER}/logger.sh" || __mys__pg_fail "Loading log helper"
if __mys_is_function_exist __mys__helper_extend; then
  __mys__helper_extend || __mys__pg_fail "Extend helper function" # preload from parent setting files
fi

__mys__pg_update "Default" "Load shell configuration"
source "${MYS_DEFAULT_FOLDER}/path.sh" || __mys__pg_fail "Loading path file"
if __mys_only_fully; then
  source "${MYS_DEFAULT_FOLDER}/variable.sh" || __mys__pg_fail "Loading variable file"
fi
source "${MYS_DEFAULT_FOLDER}/alias.sh" || __mys__pg_fail "Loading alias file"
if __mys_is_function_exist __mys__shell_extend; then
  __mys__shell_extend || __mys__pg_fail "Extend shell function" # preload from parent setting files
fi

__mys__pg_update "Personal" "Load shell configuration"
if __mys_only_fully; then
  source "${MYS_PERSONAL_FOLDER}/path.sh" || __mys__pg_fail "Loading personal path"
  source "${MYS_PERSONAL_FOLDER}/variable.sh" || __mys__pg_fail "Loading personal variable"
fi
source "${MYS_PERSONAL_FOLDER}/alias.sh" || __mys__pg_fail "Loading alias file"

__mys__pg_stop
