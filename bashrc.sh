# shellcheck disable=SC1090,SC1091,SC2148

# export MYS_TYPE="CI" # FULLY || SSH

export MYS_SHELL_TYPE='BASH'
export MYS_SHELL_TYPE_LOW="bash"

export MYS_RC_NAME='.bashrc'
export _MYS_RC_NAME="bashrc.sh"

__mys__helper_extend() {
  info "Hello from bash"
}
export -f __mys__helper_extend &>/dev/null

source .shellrc.sh || echo "rc file not exist" >&2
