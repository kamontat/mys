# shellcheck disable=SC1090,SC2148

# Theme setting
export __MYS_PG_SUCCESS_COLOR="${MYS_SETTING_PROGRESS_SUCCESS_COLOR:-32}"
export _MYS_PG_SUCCESS="\033[1;${__MYS_PG_SUCCESS_COLOR}m"
export __MYS_PG_ERROR_COLOR="${MYS_SETTING_PROGRESS_ERROR_COLOR:-31}"
export _MYS_PG_ERROR="\033[1;${__MYS_PG_ERROR_COLOR}m"
export __MYS_PG_TIME_COLOR="${MYS_SETTING_PROGRESS_TIME_COLOR:-33}"
export _MYS_PG_TIME="\033[1;${__MYS_PG_TIME_COLOR}m"

export _MYS_PG_RESET="\033[0m"

# Progress setting
export _MYS_PG_FORMAT_TIME=${MYS_SETTING_PROGRESS_FORMAT_TIME:-true}
export _MYS_PG_STYLE="${MYS_SETTING_PROGRESS_SYMBOL:-'arrow'}"
export _MYS_PG_SHOW_PERF_INFO="${MYS_SETTING_PROGRESS_SHOW_PERF:-false}"

export _MYS_PG_MESSAGE_LENGTH=35
export _MYS_PG_COUNT=1

__mys__pg_sec() {
  if command -v "gdate" &>/dev/null; then
    gdate +%s%3N
  else
    echo "0"
  fi
}

__mys__pg_conv_time() {
  ! $_MYS_PG_FORMAT_TIME && echo "$1" && return 0

  local ms="$1"
  printf '%0dm:%0ds:%0dms' $((ms / 60000)) $((ms % 60000 / 1000)) $((ms % 1000))
}

__mys__pg_format_message() {
  local title="$1"
  shift
  local message="$*"
  printf "(%-10s) %s" "$title" "$message"
}

__mys__pg_message() {
  local color symbol raw_time dur message

  color="$1"
  symbol="$2"
  raw_time="$3"
  shift 3

  dur="$(__mys__pg_conv_time "${raw_time}")"
  message="$*"

  printf "${color}[%s]${_MYS_PG_RESET} %-${_MYS_PG_MESSAGE_LENGTH}s done in ${_MYS_PG_TIME}%s${_MYS_PG_RESET}." "$symbol" "$message" "$dur"
}

__mys__pg_show_message_by() {
  if $1; then
    __mys__pg_completed_message "$2" "$3"
  else
    __mys__pg_failure_message "$2" "$3"
  fi
  echo
}

__mys__pg_completed_message() {
  local dur="$1"
  shift
  local message="$*"

  __mys__pg_message "$_MYS_PG_SUCCESS" "+" "$dur" "$message"
}

__mys__pg_failure_message() {
  local dur="$1"
  shift
  local message="$*"

  __mys__pg_message "$_MYS_PG_ERROR" "-" "$dur" "$message"
}

export PG_START_TIME
PG_START_TIME="$(__mys__pg_sec)"

export PG_PREV_TIME
PG_PREV_TIME="$(__mys__pg_sec)"

export PG_PREV_MSG
PG_PREV_MSG=$(__mys__pg_format_message "Start" "Initialization")

export PG_PREV_STATE
PG_PREV_STATE=true

__mys__pg_start() {
  local msg
  msg=${1:-"Initialization shell. Please Wait..."}
  "${MYS_LIB_FOLDER}/revolver" start -s "$_MYS_PG_STYLE" "${msg}"
}

__mys__pg_update() {
  TIME=$(($(__mys__pg_sec) - PG_PREV_TIME))

  "${MYS_LIB_FOLDER}/revolver" update "$2.."

  if ! $PG_PREV_STATE || "$_MYS_PG_SHOW_PERF_INFO"; then
    __mys__pg_show_message_by "$PG_PREV_STATE" "$TIME" "$PG_PREV_MSG"
  fi

  PG_PREV_TIME=$(__mys__pg_sec)
  PG_PREV_MSG=$(__mys__pg_format_message "$@")
  PG_PREV_STATE=true
  ((_MYS_PG_COUNT++))
}

__mys__pg_fail() {
  PG_PREV_STATE=false
  PG_PREV_MSG=$(__mys__pg_format_message "Error" "$1")
}

__mys__pg_stop() {
  TIME=$(($(__mys__pg_sec) - PG_PREV_TIME))

  if "$PG_SHOW_PERF_INFO"; then
    __mys__pg_show_message_by "$PG_PREV_STATE" "$TIME" "$PG_PREV_MSG"
  fi

  "${MYS_LIB_FOLDER}/revolver" stop

  TIME=$(($(__mys__pg_sec) - PG_START_TIME))

  printf "${_MYS_PG_SUCCESS}[+]${_MYS_PG_RESET} %-${_MYS_PG_MESSAGE_LENGTH}s      in ${_MYS_PG_TIME}%s${_MYS_PG_RESET}." "$(__mys__pg_format_message "Completed" "Initialization $_MYS_PG_COUNT tasks")" "$(__mys__pg_conv_time "${TIME}")"
  echo
}
