# shellcheck disable=SC1090,SC2148,SC2154

# Usage:
#        - debug
#        - info, information
#        - notice, notification
#        - warn, warning
#        - err, error

## Description {{{
#
# Logger for shell script.
#
# Homepage: https://github.com/rcmdnk/shell-logger
#
_MYS_LOGGER_NAME="shell-logger"
_MYS_LOGGER_VERSION="v0.2.0"
_MYS_LOGGER_DATE="04/Feb/2019"
# }}}

## License {{{
#
#MIT License
#
#Copyright (c) 2017 rcmdnk
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
# }}}

# Default variables {{{
MYS_LOGGER_DATE_FORMAT=${MYS_SETTING_LOGGER_DATE_FORMAT:-'%Y/%m/%d %H:%M:%S'}
MYS_LOGGER_LEVEL=${MYS_SETTING_LOGGER_LEVEL:-1} # 0: debug, 1: info, 2: notice, 3: warning, 4: error
MYS_LOGGER_STDERR_LEVEL=${MYS_SETTING_LOGGER_STDERR_LEVEL:-3}
MYS_LOGGER_DEBUG_COLOR=${MYS_SETTING_LOGGER_DEBUG_COLOR:-"3"}
MYS_LOGGER_INFO_COLOR=${MYS_SETTING_LOGGER_INFO_COLOR:-"32"}
MYS_LOGGER_NOTICE_COLOR=${MYS_SETTING_LOGGER_NOTICE_COLOR:-"36"}
MYS_LOGGER_WARNING_COLOR=${MYS_SETTING_LOGGER_WARNING_COLOR:-"33"}
MYS_LOGGER_ERROR_COLOR=${MYS_SETTING_LOGGER_ERROR_COLOR:-"31"}
MYS_LOGGER_COLOR=${MYS_SETTING_LOGGER_COLOR:-auto}
MYS_LOGGER_COLORS=("$MYS_LOGGER_DEBUG_COLOR" "$MYS_LOGGER_INFO_COLOR" "$MYS_LOGGER_NOTICE_COLOR" "$MYS_LOGGER_WARNING_COLOR" "$MYS_LOGGER_ERROR_COLOR")
if [ "${MYS_SETTING_LOGGER_LEVELS}" = "" ]; then
  MYS_LOGGER_LEVELS=("DEBUG" "INFO" "NOTICE" "WARNING" "ERROR")
fi
MYS_LOGGER_SHOW_TIME=${MYS_SETTING_LOGGER_SHOW_TIME:-1}
MYS_LOGGER_SHOW_FILE=${MYS_SETTING_LOGGER_SHOW_FILE:-1}
MYS_LOGGER_SHOW_LEVEL=${MYS_SETTING_LOGGER_SHOW_LEVEL:-1}
MYS_LOGGER_ERROR_RETURN_CODE=${MYS_SETTING_LOGGER_ERROR_RETURN_CODE:-100}
MYS_LOGGER_ERROR_TRACE=${MYS_SETTING_LOGGER_ERROR_TRACE:-1}

MYS_LOGGER_TO_FILE=${MYS_SETTING_LOGGER_TO_FILE:-true}
MYS_LOGGER_FOLDER=${MYS_SETTING_LOGGER_FOLDER:-'/tmp/mys'}
MYS_LOGGER_FILE_NAME=${MYS_SETTING_LOGGER_FILE_NAME:-'stdout.log'}
MYS_LOGGER_ERROR_FILE_NAME=${MYS_SETTING_LOGGER_ERROR_FILE_NAME:-'stderr.log'}
# }}}

# Other global variables {{{
_MYS_LOGGER_WRAP=0
#}}}

# Functions {{{
_logger_version() {
  printf "%s %s %s\\n" "$_MYS_LOGGER_NAME" "$_MYS_LOGGER_VERSION" "$_MYS_LOGGER_DATE"
}

_get_level() {
  if [ $# -eq 0 ]; then
    local level=1
  else
    local level=$1
  fi
  if ! expr "$level" : '[0-9]*' >/dev/null; then
    [ -z "$ZSH_VERSION" ] || emulate -L ksh
    local i=0
    while [ $i -lt ${#MYS_LOGGER_LEVELS[@]} ]; do
      if [ "$level" = "${MYS_LOGGER_LEVELS[$i]}" ]; then
        level=$i
        break
      fi
      ((i++))
    done
  fi
  echo $level
}

_logger_level() {
  [ "$MYS_LOGGER_SHOW_LEVEL" -ne 1 ] && return
  if [ $# -eq 1 ]; then
    local level=$1
  else
    local level=1
  fi
  [ -z "$ZSH_VERSION" ] || emulate -L ksh
  printf "[%8s ]" "${MYS_LOGGER_LEVELS[$level]}"
}

_logger_time() {
  [ "$MYS_LOGGER_SHOW_TIME" -ne 1 ] && return
  printf "[%s]" "$(date +"$MYS_LOGGER_DATE_FORMAT")"
}

_logger_file() {
  [ "$MYS_LOGGER_SHOW_FILE" -ne 1 ] && return
  local i=0
  if [ $# -ne 0 ]; then
    i=$1
  fi
  if [ -n "$BASH_VERSION" ]; then
    printf " -- %s:%s" "${BASH_SOURCE[$((i + 1))]}" "${BASH_LINENO[$i]}"
  else
    emulate -L ksh
    printf " -- %s" "${funcfiletrace[$i]}"
  fi
}

_logger() {
  local wrap msg level out
  local _output_message filename

  ((_MYS_LOGGER_WRAP++))

  wrap=${_MYS_LOGGER_WRAP}
  _MYS_LOGGER_WRAP=0
  if [ $# -eq 0 ]; then
    return
  fi
  level="$1"
  shift
  if [ "$level" -lt "$(_get_level "$MYS_LOGGER_LEVEL")" ]; then
    return
  fi
  msg="$(_logger_time) $(_logger_level "$level") $* $(_logger_file "$wrap")"

  out=1
  filename="${MYS_LOGGER_FILE_NAME}"
  if [ "$level" -ge "$MYS_LOGGER_STDERR_LEVEL" ]; then
    out=2
    filename="${MYS_LOGGER_ERROR_FILE_NAME}"
  fi

  _output_message=''
  if [ "$MYS_LOGGER_COLOR" = "always" ] || { test "$MYS_LOGGER_COLOR" = "auto" && test -t $out; }; then
    [ -z "$ZSH_VERSION" ] || emulate -L ksh
    _output_message=$(printf "\e[${MYS_LOGGER_COLORS[$level]}m%s\e[m" "$msg")
  else
    _output_message=$(printf "%s" "$msg")
  fi
  if [ "$MYS_LOGGER_TO_FILE" = true ]; then
    mkdir -p "${MYS_LOGGER_FOLDER}" &>/dev/null # create temp folder if not exist
    if ! test -f "${MYS_LOGGER_FOLDER%%/}/${MYS_LOGGER_FILE_NAME}"; then
      touch "${MYS_LOGGER_FOLDER%%/}/${MYS_LOGGER_FILE_NAME}" # create log file
    fi
    if ! test -f "${MYS_LOGGER_FOLDER%%/}/${MYS_LOGGER_ERROR_FILE_NAME}"; then
      touch "${MYS_LOGGER_FOLDER%%/}/${MYS_LOGGER_ERROR_FILE_NAME}" # create error file
    fi

    printf '%s\n' "$_output_message" >>"${MYS_LOGGER_FOLDER%%/}/${filename}"
  else
    if [[ "$out" == "1" ]]; then
      printf '%s\n' "$_output_message"
    else
      printf '%s\n' "$_output_message" >&2
    fi
  fi
}

debug() {
  ((_MYS_LOGGER_WRAP++))
  _logger 0 "$*"
}

information() {
  ((_MYS_LOGGER_WRAP++))
  _logger 1 "$*"
}
info() {
  ((_MYS_LOGGER_WRAP++))
  information "$*"
}

notification() {
  ((_MYS_LOGGER_WRAP++))
  _logger 2 "$*"
}
notice() {
  ((_MYS_LOGGER_WRAP++))
  notification "$*"
}

warning() {
  ((_MYS_LOGGER_WRAP++))
  _logger 3 "$*"
}
warn() {
  ((_MYS_LOGGER_WRAP++))
  warning "$*"
}

error() {
  local first current_source i last_source
  local file line func func_call

  ((_MYS_LOGGER_WRAP++))
  if [ "$MYS_LOGGER_ERROR_TRACE" -eq 1 ]; then
    {
      [ -z "$ZSH_VERSION" ] || emulate -L ksh
      first=0
      if [ -n "$BASH_VERSION" ]; then
        current_source=$(echo "${BASH_SOURCE[0]##*/}" | cut -d"." -f1)
        func="${FUNCNAME[1]}"
        i=$((${#FUNCNAME[@]} - 2))
      else
        current_source=$(echo "${funcfiletrace[0]##*/}" | cut -d":" -f1 | cut -d"." -f1)
        func="${funcstack[1]}"
        i=$((${#funcstack[@]} - 1))
        last_source=${funcfiletrace[$i]%:*}
        if [ "$last_source" = zsh ]; then
          ((i--))
        fi
      fi
      if [ "$current_source" = "shell-logger" ] && [ "$func" = err ]; then
        first=1
      fi
      if [ $i -ge $first ]; then
        echo "Traceback (most recent call last):"
      fi
      while [ $i -ge $first ]; do
        if [ -n "$BASH_VERSION" ]; then
          file=${BASH_SOURCE[$((i + 1))]}
          line=${BASH_LINENO[$i]}
          func=""
          if [ ${BASH_LINENO[$((i + 1))]} -ne 0 ]; then
            if [ "${FUNCNAME[$((i + 1))]}" = "source" ]; then
              func=", in ${BASH_SOURCE[$((i + 2))]}"
            else
              func=", in ${FUNCNAME[$((i + 1))]}"
            fi
          fi
          func_call="${FUNCNAME[$i]}"
          if [ "$func_call" = "source" ]; then
            func_call="${func_call} ${BASH_SOURCE[$i]}"
          else
            func_call="${func_call}()"
          fi
        else
          file=${funcfiletrace[$i]%:*}
          line=${funcfiletrace[$i]#*:}
          func=""
          if [ -n "${funcstack[$((i + 1))]}" ]; then
            if [ "${funcstack[$((i + 1))]}" = "${funcfiletrace[$i]%:*}" ]; then
              func=", in ${funcfiletrace[$((i + 1))]%:*}"
            else
              func=", in ${funcstack[$((i + 1))]}"
            fi
          fi
          func_call="${funcstack[$i]}"
          if [ "$func_call" = "${funcfiletrace[$((i - 1))]%:*}" ]; then
            func_call="source ${funcfiletrace[$((i - 1))]%:*}"
          else
            func_call="${func_call}()"
          fi
        fi
        echo "  File \"${file}\", line ${line}${func}"
        if [ $i -gt $first ]; then
          echo "    $func_call"
        else
          echo ""
        fi
        ((i--))
      done
    } 1>&2
  fi
  _logger 4 "$*"
  return "$MYS_LOGGER_ERROR_RETURN_CODE"
}
err() {
  ((_MYS_LOGGER_WRAP++))
  error "$*"
}
# }}}
