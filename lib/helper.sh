# shellcheck disable=SC1090,SC2148

export __mys_is_command_exist
__mys_is_command_exist() {
  command -v "$1" &>/dev/null
}

export __mys_is_function_exist
__mys_is_function_exist() {
  type "$1" &>/dev/null
}

export __mys_is_file_exist
__mys_is_file_exist() {
  test -f "$1"
}

export __mys_is_folder_exist
__mys_is_folder_exist() {
  test -d "$1"
}

export __mys_is_string_exist
__mys_is_string_exist() {
  test -n "$1"
}

export __mys_only_fully
__mys_only_fully() {
  test "$MYS_TYPE" = "FULLY"
}

export __mys_only_ssh
__mys_only_ssh() {
  test "$MYS_TYPE" = "SSH"
}

export __mys_only_bash
__mys_only_bash() {
  test "$MYS_SHELL_TYPE" = "BASH"
}

export __mys_only_zsh
__mys_only_zsh() {
  test "$MYS_SHELL_TYPE" = "ZSH"
}

export __mys_only_mac
__mys_only_mac() {
  [[ "$OSTYPE" == "darwin"* ]]
}
