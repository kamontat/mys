# shellcheck disable=SC1090,SC1091,SC2148

# export MYS_TYPE="CI" # FULLY || SSH

export MYS_SHELL_TYPE='ZSH'
export MYS_SHELL_TYPE_LOW="zsh"

export MYS_RC_NAME='.zshrc'
export _MYS_RC_NAME="zshrc.sh"

__mys__shell_extend() {
  info "Hello from zsh"
}
export -f __mys__shell_extend &>/dev/null

source .shellrc.sh || echo "rc file not exist" >&2
