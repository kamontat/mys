# shellcheck disable=SC1090,SC2148,SC2154

export PATH="/bin"                  # bin
export PATH="/sbin:$PATH"           # sbin

export PATH="/usr/bin:$PATH"        # user bin
export PATH="/usr/sbin:$PATH"       # user sbin

export PATH="/usr/local/bin:$PATH"  # local user bin
export PATH="/usr/local/sbin:$PATH" # local user sbin

export MANPATH="/usr/local/man:$MANPATH"
