# shellcheck disable=SC1090,SC2015,SC2148,SC2154

__mys_is_folder_exist "/usr/local/opt/openssl" &&
  export PATH="$PATH:/usr/local/opt/openssl/bin" ||
  warn "OpenSSL is not exist" # openssl

__mys_is_folder_exist "/usr/local/opt/coreutils/libexec/gnubin" &&
  export PATH="$PATH:/usr/local/opt/coreutils/libexec/gnubin" ||
  warn "Coreutils is not exist"

__mys_is_folder_exist "/usr/local/opt/coreutils/libexec/gnuman" &&
  export MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH" ||
  warn "Manpage of coreutils is not exist"

__mys_is_folder_exist "${HOME}/.rbenv/shims" &&
  export PATH="${HOME}/.rbenv/shims:$PATH" ||
  warn "rbenv is not exist"

__mys_only_mac &&
  __mys_is_folder_exist "/usr/local/MacGPG2/bin" &&
  export PATH="/usr/local/MacGPG2/bin:$PATH" ||
  warn "MacGPG2 is not exist"

__mys_is_folder_exist "/usr/local/opt/icu4c" &&
  export PATH="$PATH:/usr/local/opt/icu4c/bin:/usr/local/opt/icu4c/sbin" &&
  export PKG_CONFIG_PATH="/usr/local/opt/icu4c/lib/pkgconfig" ||
  warn "icu4c is not exist"
