# shellcheck disable=SC1090,SC2148,SC2154

__SHELL="/usr/local/bin/${MYS_SHELL_TYPE_LOW:-'bash'}"
export SHELL="${_MYS_SHELL_PATH:-$__SHELL}"

# chsh -s "$SHELL"

export ARCHFLAGS="-arch x86_64"

export KEYTIMEOUT=1

export LANG="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"

export USER="$MYZS_USER"
export DEFAULT_USER="$USER"
