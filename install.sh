#!/usr/bin/env bash
# shellcheck disable=SC1000

# generate by create-script-file v4.0.0
# link (https://github.com/Template-generator/create-script-file/tree/v4.0.0)

# set -x #DEBUG - Display commands and their arguments as they are executed.
# set -v #VERBOSE - Display shell input lines as they are read.
# set -n #EVALUATE - Check syntax of the script but don't execute.

#/ -----------------------------------
#/ Description:  ...
#/ How to:       ...
#/               ...
#/ Option:       --help | -h | -? | help | h | ?
#/                   > show this message
#/               --version | -v | version | v
#/                   > show command version
#/ -----------------------------------
#/ Create by:    Kamontat Chantrachirathunrong <kamontat.c@hotmail.com>
#/ Since:        03/04/2019
#/ -----------------------------------
#/ Error code    1      -- error
#/ -----------------------------------
#/ Known bug:    ...
#/ -----------------------------------
#// Version:      0.0.1   -- description
#//               0.0.2b1 -- beta-format
#//               0.0.2a1 -- alpha-format

__MYS_INSTALL_FOLDERNAME=".mys"
__MYS_INSTALL_BINPATH="/usr/local/bin"
__MYS_INSTALL_ROOT="$HOME"

__MYS_INSTALL_FOLDERPATH="${__MYS_INSTALL_BINPATH}/${__MYS_INSTALL_FOLDERNAME}"

printf "Copy file   -- "
mkdir -p "${__MYS_INSTALL_FOLDERPATH}"
cp zshrc.sh "${__MYS_INSTALL_FOLDERPATH}/.zshrc"
cp bashrc.sh "${__MYS_INSTALL_FOLDERPATH}/.bashrc"
cp install.sh "${__MYS_INSTALL_FOLDERPATH}/mys-install"
cp uninstall.sh "${__MYS_INSTALL_FOLDERPATH}/mys-uninstall"
cp deploy.sh "${__MYS_INSTALL_FOLDERPATH}/mys-deploy"

cp -r const "${__MYS_INSTALL_FOLDERPATH}/const"
cp -r default "${__MYS_INSTALL_FOLDERPATH}/default"
cp -r lib "${__MYS_INSTALL_FOLDERPATH}/lib"
cp -r personal "${__MYS_INSTALL_FOLDERPATH}/personal"
echo "COMPLETED"

printf "Link Zshrc  -- "
if test -f "${HOME}/.zshrc"; then
  mv "${HOME}/.zshrc" "${HOME}/.zshrc.before"
fi
ln -s "${__MYS_INSTALL_FOLDERPATH}/.zshrc" "${HOME}/.zshrc"
echo "COMPLETED"

printf "Link Bashrc -- "
if test -f "${HOME}/.bashrc"; then
  mv "${HOME}/.bashrc" "${HOME}/.bashrc.before"
fi
ln -s "${__MYS_INSTALL_FOLDERPATH}/.bashrc" "${HOME}/.bashrc"
echo "COMPLETED"
