# shellcheck disable=SC1090,SC2034,SC2148

# ----------------------------------- #
# Loading settings                    #
# ----------------------------------- #

# ----------------------------------- #
# Logger                              #
# ----------------------------------- #

# MYS_SETTING_LOGGER_LEVEL=1 # 0: debug, 1: info, 2: notice, 3: warning, 4: error
MYS_SETTING_LOGGER_TO_FILE=true
# MYS_SETTING_LOGGER_FOLDER='/tmp/mys'
# MYS_SETTING_LOGGER_FILE_NAME='stdout.log'
# MYS_SETTING_LOGGER_ERROR_FILE_NAME='stderr.log'

# MYS_SETTING_LOGGER_DEBUG_COLOR="3"
# MYS_SETTING_LOGGER_INFO_COLOR=""
# MYS_SETTING_LOGGER_NOTICE_COLOR="36"
# MYS_SETTING_LOGGER_WARNING_COLOR="33"
# MYS_SETTING_LOGGER_ERROR_COLOR="31"
# MYS_SETTING_LOGGER_COLOR='auto'

# ----------------------------------- #
# Progress                            #
# ----------------------------------- #

MYS_SETTING_PROGRESS='auto' # auto || long || none # TODO: implement this
MYS_SETTING_PROGRESS_SHOW_PERF=false
MYS_SETTING_PROGRESS_TIME=false
MYS_SETTING_PROGRESS_FORMAT_TIME=true

MYS_SETTING_PROGRESS_SYMBOL='dots'

# MYS_SETTING_PROGRESS_MESSAGE_COLOR=242
# MYS_SETTING_PROGRESS_SYMBOL_COLOR=33
# MYS_SETTING_PROGRESS_SUCCESS_COLOR='32'
# MYS_SETTING_PROGRESS_TIME_COLOR='33'
# MYS_SETTING_PROGRESS_ERROR_COLOR='31'
